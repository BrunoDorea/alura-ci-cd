<h1 align="center">
  Gitlab CI e Docker: Pipeline de entrega contínua
</h1>

<p align="center">
  <a href="https://wakatime.com/badge/user/68660678-6b86-4b78-98df-f5f41a37e1bc/project/22ff1fcb-ab96-4c23-b7e9-b87d20fa3461"><img src="https://wakatime.com/badge/user/68660678-6b86-4b78-98df-f5f41a37e1bc/project/22ff1fcb-ab96-4c23-b7e9-b87d20fa3461.svg" alt="wakatime"></a>
</p>

<p align="center">
  <a href="#-projeto">🖥️ Projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-tecnologias">🚀 Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-license">📝 License</a>
</p>

## 💻 Projetos

Repositório para o curso de "Gitlab CI e Docker: Pipeline de entrega contínua" da Alura.

## 🚀 Tecnologias

<p align="center">
  <img src="">
</p>

## 📝 License

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE) para mais detalhes.

---
